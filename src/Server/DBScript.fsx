#r "../../libraries/FSharp.Data.SqlProvider.dll"
//#r "lib/FSharp.Data.SqlProvider.dll"


open System
open FSharp.Data.Sql

FSharp.Data.Sql.Common.QueryEvents.SqlQueryEvent |> Event.add (printfn "Executing SQL: %O")

[<Literal>]
let connStr = "Server = localhost; Database = SAFEPersistentChat; User Id = SA; Password = SAFEPersistent-Chat;"


type sql = SqlDataProvider<Common.DatabaseProviderTypes.MSSQLSERVER, connStr>

let ctx = sql.GetDataContext()

let getUser (nick : string) = //Hopefully it will return 0 or 1 element
    query { //SELECT * FROM users WHERE nick = nick;
        for user in ctx.Dbo.Users do
            where (user.Nick = nick)
            select user
    } 
    |> Seq.toArray

let getUserId (nick : string) = 
    let user = (getUser nick)
    match user.Length with
    | 0 -> 0
    | _ -> (int) user.[0].Id

let anonymousLogin (nick : string) = 
    if (getUser nick).Length = 0 then  //New user
        //printf "user doesn't exists, creating user for %s \n" nick//Anonymous {nick = nick; status = ""; imageUrl = imageUrl}
        let newUser = ctx.Dbo.Users.Create() //INSERT INTO users (nick, status, avatar) VALUES ('?', '?', '?');
        newUser.Nick <- nick
        newUser.Status <- ""
        newUser.Avatar <- ""

    //else //User already exists
        //printf "user already created: %s %s %s" (getUser nick).[0].Nick (getUser nick).[0].Status (getUser nick).[0].Avatar //Anonymous {nick = thisUser.[0].Nick; status = thisUser.[0].Status; imageUrl = thisUser.[0].Avatar}
    ctx.SubmitUpdates()

let updateNick (nick : string) (newNick : string) = //UPDATE users SET nick = '?' WHERE nick = '?'
    getUser nick |> Array.iter(fun user -> user.Nick <- newNick)
    ctx.SubmitUpdates()
let updateStatus  (nick : string) (newStatus : string) = //UPDATE users SET status = '?' WHERE nick = '?'
    getUser nick |> Array.iter(fun user -> user.Status <- newStatus)
    ctx.SubmitUpdates()

let updateAvatar  (nick : string) (newAvatar : string) = //UPDATE users SET avatar = '?' WHERE nick = '?'
    getUser nick |> Array.iter(fun user -> user.Avatar <- newAvatar)
    ctx.SubmitUpdates()

let newChannel (channelName : string) = //INSERT INTO channels (name) VALUES ('?');
    let channel = ctx.Dbo.Channels.Create()
    channel.Name <- channelName
    ctx.SubmitUpdates()

let deleteChMsgs (channelName : string) =
    query { //DELETE FROM channels_messages WHERE channel_name = '?'
        for channelMsg in ctx.Dbo.ChannelsMessages do
            where (channelMsg.ChannelName = channelName)
    } |> Seq.``delete all items from single table`` |> Async.RunSynchronously
let deleteChannel (channelName : string) =
    let a = deleteChMsgs channelName
    query { //DELETE FROM channels WHERE name = '?'
        for channel in ctx.Dbo.Channels do
            where (channel.Name = channelName)
    } |> Seq.``delete all items from single table`` |> Async.RunSynchronously

let userJoinedCh (nick : string) (channelName : string) = 
    let newUsrCh = ctx.Dbo.UsersChannels.Create()
    newUsrCh.UserId <- getUserId nick
    newUsrCh.ChannelName <- channelName //SEARCH CHANNEL ID WITH NAME
    ctx.SubmitUpdates()

let userLeftCh (nick :string) (chName : string) = //DELETE FROM users_channels WHERE user_id = '?' AND channel_name = '?'
    query { //DELETE FROM channels_messages WHERE channel_name = '?'
        for userCh in ctx.Dbo.UsersChannels do
            where (userCh.UserId = getUserId nick && userCh.ChannelName = chName) 
    } |> Seq.``delete all items from single table`` |> Async.RunSynchronously

let userSentMsg (nick : string) (chName : string) (msg : string) = 
    let newMsg = ctx.Dbo.ChannelsMessages.Create()
    newMsg.ChannelName <- chName // TODO: SEARCH CHANNEL ID WITH NAME
    newMsg.UserId <- getUserId nick
    newMsg.Message <- msg
    newMsg.Dth <- System.DateTime.Now
    ctx.SubmitUpdates()

let getChannels () = ctx.Dbo.Channels |> Seq.toArray // TODO: TRANSFORM IN AN ARRAY OF CHANNELS?

let getAllUsers () = ctx.Dbo.Users |> Seq.toArray // TODO: TRANSFORM IN AN ARRAY OF USERS?

let getChMsgs (channel : string) = 
    query{
        for msg in ctx.Dbo.ChannelsMessages do
            where (msg.ChannelName = channel)
            select msg
    }|> Seq.toArray

//TEST DUMMIES

let firstNick = "MrSatan"
let secondNick = "MrAngel"
let dummy_chat = "Heaven Channel"
let dummy_msg = "Hello Everyone"


anonymousLogin firstNick
(*
printf "%s id: %d\n" firstNick (getUserId firstNick)
updateNick firstNick secondNick
printf "%s id: %d\n" secondNick (getUserId secondNick)
newChannel dummy_chat
userJoinedCh secondNick dummy_chat
userSentMsg secondNick dummy_chat dummy_msg


updateNick secondNick firstNick 
*)
//TODO for msg in (getChMsgs "Heaven Channel") do printf "%s" msg