﻿module DBComm
//#r "lib/FSharp.Data.SqlProvider.dll"


open System
open FSharp.Data.Sql
open ChatTypes


[<Literal>]
let connStr = "Server = localhost; Database = SAFEPersistentChat; User Id = SA; Password = SAFEPersistent-Chat;"

type sql = SqlDataProvider<ConnectionString = connStr, DatabaseVendor = Common.DatabaseProviderTypes.MSSQLSERVER>

let ctx  = sql.GetDataContext()

let getUser (nick : string) = //Hopefully it will return 0 or 1 element
    query { //SELECT * FROM users WHERE nick = nick;
        for user in ctx.Dbo.Users do
            where (user.Nick = nick)
            select user
    } 
    |> Seq.toArray

let getUserId (nick : string) = 
    let user = (getUser nick)
    match user.Length with
    | 0 -> 0
    | _ -> (int) user.[0].Id

let anonymousLogin (nick : string) (id : int) = 
    if (getUser nick).Length = 0 then  //user doesn't exists, creating new user for 'nick'
        //INSERT INTO users (nick, status, avatar) VALUES ('?', '?', '?');
        let newUser = ctx.Dbo.Users.Create()
        newUser.Id <- id
        newUser.Nick <- nick
        newUser.Status <- ""
        newUser.Avatar <- ""
        
        
        ctx.SubmitUpdates()

    //else //User already exists

let updateNick (nick : string) (newNick : string) = //UPDATE users SET nick = '?' WHERE nick = '?'
    getUser nick |> Array.iter(fun user -> user.Nick <- newNick)
    ctx.SubmitUpdates()
let updateStatus  (nick : string) (newStatus : string) = //UPDATE users SET status = '?' WHERE nick = '?'
    getUser nick |> Array.iter(fun user -> user.Status <- newStatus)
    ctx.SubmitUpdates()
let updateAvatar  (nick : string) (newAvatar : string) = //UPDATE users SET avatar = '?' WHERE nick = '?'
    getUser nick |> Array.iter(fun user -> user.Avatar <- newAvatar)
    ctx.SubmitUpdates()

let newChannel (channelName : string) = //INSERT INTO channels (name) VALUES ('?');
    let channel = ctx.Dbo.Channels.Create()
    channel.Name <- channelName
    ctx.SubmitUpdates()

let deleteChMsgs (channelID : int) = //DELETE FROM channels_messages WHERE channel_name = '?'
    query { 
        for channelMsg in ctx.Dbo.ChannelsMessages do
            where (channelMsg.ChannelId = channelID)
    } |> Seq.``delete all items from single table`` |> Async.RunSynchronously
let deleteChannel (channelID : int) =
    let a = deleteChMsgs channelID
    query { //DELETE FROM channels WHERE name = '?'
        for channel in ctx.Dbo.Channels do
            where (channel.Id = channelID)
    } |> Seq.``delete all items from single table`` |> Async.RunSynchronously


let userJoinedCh (user_id : UserId) (channelName : string) = 
    let newUsrCh = ctx.Dbo.UsersChannels.Create()
    //newUsrCh.UserId <- getUserId user_id
    newUsrCh.ChannelName <- channelName //SEARCH CHANNEL ID WITH NAME
    ctx.SubmitUpdates()

let userLeftCh (nick :string) (chName : string) = //DELETE FROM users_channels WHERE user_id = '?' AND channel_name = '?'
    query { 
        for userCh in ctx.Dbo.UsersChannels do
            where (userCh.UserId = getUserId nick && userCh.ChannelName = chName) 
    } |> Seq.``delete all items from single table`` |> Async.RunSynchronously

let userSentMsg (tempID : string) (msg : string) (chId : string) = 
    let channel_id = chId |> int
    if channel_id > 103 then
        let newMsg = ctx.Dbo.ChannelsMessages.Create()
        newMsg.ChannelId <- chId |> int
        newMsg.UserId <- tempID |> int
        newMsg.Message <- (string) msg
        newMsg.Dth <- System.DateTime.Now
        ctx.SubmitUpdates()
        
let getChannels () = ctx.Dbo.Channels |> Seq.toArray // TODO: TRANSFORM IN AN ARRAY OF CHANNELS?

let getAllUsers () = ctx.Dbo.Users |> Seq.toArray // TODO: TRANSFORM IN AN ARRAY OF USERS?

let getChMsgs (chId : int) = 
        let channel_id = chId
        query{
        for msg in ctx.Dbo.ChannelsMessages do
            where (msg.ChannelId = channel_id)
            select msg
        }|> Seq.toArray
let getUsersLastId : int =  100 // TODO: GET ON TABLE

